#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicionario = {}
    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        cliente = ''
        dir_sip = self.client_address[0]
        dir_ip = self.client_address[1]
        self.dicionario[cliente]={dir_sip,dir_ip}

        for line in self.rfile:
            print("El cliente nos manda ", str(line.decode('utf-8')))
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        print(str(self.client_address))

if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    puerto = int(sys.argv[1])
    serv = socketserver.UDPServer(('', puerto), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
